;; Lots of things I can twiddle depending on how much I feel
;; like pretending other people observe netiquette. =)
(setq gnus-treat-fill-article nil)
(setq gnus-treat-fill-long-lines nil)
(setq gnus-treat-capitalize-sentences nil)
(setq gnus-treat-date-local 'head)
(setq gnus-treat-hide-headers 'head)
(setq gnus-treat-hide-boring-headers 'head)
(setq gnus-treat-date-english t)

(setq gnus-boring-article-headers '(empty followup-to reply-to to-address date long-to many-to))

;; I browse by thread, so I tend to remember thread context; if I need
;; more info, I can just unhide cited text.
(setq gnus-treat-hide-citation t)


;; autosave files...
(setq gnus-save-newsrc-file t)
(setq bbdb-notice-auto-save-file t)

;; extra headers, To does not appears by default...
(setq gnus-extra-headers '(To X-NextAction X-Waiting))
(setq nnmail-extra-headers gnus-extra-headers)

;; uso de cache
(setq gnus-use-cache t)

;; filtra luego de recibirlos/sincronizarlos todos
(setq nnmail-resplit-incoming t)

;; filter crud
(setq nnimap-split-inbox "INBOX")
;; (setq nnimap-split-predicate "UNDELETED") ;; commented; By default is UNSEEN and UNDELETED, so the mark "UNSEEN" keeps in sync
(setq nnimap-split-crosspost nil)

;; for searching...
(require 'nnir)

(setq mail-from-style "angles") ;; Name <name@example.com>

(setq gnus-select-method '(nnimap "gnuchile"
                                  (remove-prefix "INBOX.")
                                  (nnimap-address "mail.gnuchile.cl")
                                  ;;(nnimap-server-port 1430)
                                  (nnimap-stream ssl)
                                  (nnimap-list-pattern ("INBOX" "mail/*" "inbox"))
                                  (nnimap-authinfo-file "~/.authinfo")
                                  (nnir-search-engine imap)
                                  )
      )

(setq gnus-secondary-select-methods
           '(
             ;; (nnimap "gnuchile"
             ;;         (remove-prefix "INBOX.")
             ;;         (nnimap-address "mail.gnuchile.cl")
             ;;         ;;(nnimap-server-port 1430)
             ;;         (nnimap-stream ssl)
             ;;         (nnimap-list-pattern ("INBOX" "mail/*" ))
             ;;         (nnimap-authinfo-file "~/.authinfo")
             ;;         )
             ;(nnimap "simpleserver") ; no special configuration
             ; perhaps a ssh port forwarded server:
             ; a UW server running on localhost
             ;; (nnimap "barbar"
                     ;; (nnimap-server-port 143)
                     ;; (nnimap-address "localhost")
                     ;; (nnimap-list-pattern ("INBOX" "mail/*")))
             ; anonymous public cyrus server:
             ;; (nnimap "cyrus.andrew.cmu.edu"
                     ;; (nnimap-authenticator anonymous)
                     ;; (nnimap-list-pattern "archive.*")
                     ;; (nnimap-stream network))
             ; a ssl server on a non-standard port:
             ;; (nnimap "vic20"
                     ;; (nnimap-address "vic20.somewhere.com")
                     ;; (nnimap-server-port 9930)
                     ;; (nnimap-stream ssl))
             
             ;; FSFLA
             (nnimap "fsfla"
                     ;; (remove-prefix "INBOX.")
                     (nnimap-address "mail.tsolar.cl")
                     ;(nnimap-server-port 1430)
                     (nnimap-stream ssl)
                     (nnimap-list-pattern ("INBOX" "mail/*"))
                     (nnimap-authinfo-file "~/.authinfo")
                     (nnir-search-engine imap)
                     )
             
             ;; Lavabit
             (nnimap "lavabit"
                     ;; (remove-prefix "INBOX.")
                     (nnimap-address "lavabit.com")
                     ;(nnimap-server-port 1430)
                     (nnimap-stream ssl)
                     (nnimap-list-pattern ("INBOX" "mail/*"))
                     (nnimap-authinfo-file "~/.authinfo")
                     (nnir-search-engine imap)
                     )
             
             ;; parabola
             (nnimap "parabola"
                     ;; (remove-prefix "INBOX.")
                     (nnimap-address "mail.parabolagnulinux.org")
                     (nnimap-server-port 993)
                     (nnimap-stream ssl)
                     (nnimap-list-pattern ("INBOX" "*"))
                     (prefix "INBOX")
                     (nnimap-authinfo-file "~/.authinfo")
                     (nnir-search-engine imap)
                     )
             )
)


;; filtros:
;; (setq nnimap-split-rule 'nnimap-split-fancy
;;       nnimap-split-fancy 
;;       '(| 
;;         ;; (: gnus-registry-split-fancy-with-parent)
;;         ;; parabola
;;         ;; (any "dev@list.parabolagnulinux.org" "parabola:INBOX.dev")
;;         ("List-Id" "dev-parabolagnulinux.org" "parabola:INBOX.dev")
;;         ("To" "dev@list.parabolagnulinux.org" "parabola:INBOX.dev")
;;         ("To" "tsolar@parabolagnulinux.org" "parabola:INBOX")        
        
;;         ;; lavabit
;;         (any "awesome@naquadah.org" "nnimap+lavabit:awesome")
;;         ("List-id" "awesome.naquadah.org" "nnimap+lavabit:awesome")
;;         ("To" "awesome@naquadag.org" "nnimap+lavabit:awesome")    
;;         ("To" "tsolar@lavabit.com" "nnimap+lavabit:Inbox")

;;         ;; fsfla
;;         ("List-id" "cons.fsfla.org" "nnimap+fsfla:cons")
;;         ("List-Id" "discusion.fsfla.org" "nnimap+fsfla:discusion")
;;         ("List-Id" "team.fsfla.org" "nnimap+fsfla:team")
;;         ("List-Id" "bobagens.fsfla.org" "nnimap+fsfla:bobagens")
;;         ("To" "bobagens@fsfla.org" "nnimap+fsfla:bobagens")
;;         ("List-Id" "legal.lists.gpl-violations.org" "nnimap+fsfla:gpl-violations")
;;         ("List-Id" "legales.fsfla.org" "nnimap+fsfla:legales")
;;         ("To" "tsolar@fsfla.org" "nnimap+fsfla:Inbox")

;;         ;; gnuchile
;;         ;; ("List-Id" "santiago-flisol.cl" "gnuchile.flisol.2010")
;;         ("To" "staff@gnuchile.cl" "staff")
;;         ("To" "directorio@gnuchile.cl" "directorio")
;;         ;; ("To" "chile@gnewbook.org" "gnewbook")
;;         ;; ("To" "santiago@listas.flisol.cl" "gnuchile.flisol.2010")
;;         ;; ("List-Id" "nacional-flisol.cl" "gnuchile.flisol.2010.nacional")
;;         ("List-Id" "coordinacion-flisol.cl" "flisol.coordinacion")
;;         ;; ("To" "devs@gnux.gnuchile.cl" "gnuchile.gnux-dev")
;;         ;; ("From" "web@santiago.flisol.cl" "gnuchile.flisol.2010.Postulaciones")
;;         ("Subject" ".*\\[CNSL\\].*" "cnsl")
;;         ("List-Id" "comunidad.lists.cnsl.cl" "cnsl")
;;         ("Subject" ".*\\[CDSL\\].*" "cdsl")
;;         ("From" "noreply@gnewbook.org" "gNewBook")
;;         ("To" "tsolar@gnuchile.cl" "Inbox")
;;         ("To" "tsolar@fundaciongnuchile.cl" "Inbox")

;;         ;; ("Subject" ".*\\[Openlibrary\\].*" "In.OL.lists.openlibrary")

;;         ;; ("To\\|Cc" "spam@nibrahim.net.in" "In.marketing")

;;         ;; (& ("From" ".*root@archive.org.*")
;;         ;;    ("Subject" ".*Nagios.*" "In.OL.Nagios-alerts"))
;;         ;; "nofiltrado"
;;         )
;;       )

;; (gnus-registry-initialize) 

;; otros filtros
(setq nnimap-split-rule
      '(("gnuchile" (".*" (
                           ("Inbox" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*tsolar@\\([f][u][n][d][a][c][i][o][n]\\)?gnuchile.cl.*")
			   ;; ("Inbox" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*tsolar@gnuchile.cl.*")
                           ("cnsl"  "^List-Id:.*comunidad.lists.cnsl.cl.*")
                           ("flisol.santiago"  "^List-Id:.*santiago-flisol.cl.*")
                           ("cnsl"  "^\\([Tt][Oo]\\|[Cc][Cc]\\):.*\\[CNSL\\].*")
                           ("cdsl"  "^\\([Tt][Oo]\\|[Cc][Cc]\\):.*\\[CDSL\\].*")
                           ("staff" "^\\([Tt][Oo]\\|[Cc][Cc]\\):.*staff@gnuchile.cl.*")
                           ("otros" "")
                           )
                     )
         )
        ("fsfla" (".*" (
                        ("cons" "^List-Id:.*cons.fsfla.org.*")
                        ("team" "^List-Id:.*team.fsfla.org.*")
                        ("info" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*info@fsfla.org.*")
                        ("Inbox" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*tsolar@fsfla.org.*")
                        ("otros" "")
                        )
                  )
         )
        ("lavabit" (".*" (
                        ("awesome" "^List-Id:.*awesome.naquadah.org.*")
                        ("Inbox" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*tsolar@lavabit.com.*")
                        ("otros" "")
                        )
                    )
         )
        ("parabola" (".*" (
                           ("INBOX.dev" "^List-Id:.*dev-parabolagnulinux.org.*")
                           ("INBOX" "^\\([Tt][Oo]\\|[Cc][Cc]\\):[:space:]*.*tsolar@parabolagnulinux.org.*")
                           ("INBOX.otros" "")
                           ))
         
         )
        ;; ("my2server"    ("INBOX" nnimap-split-fancy))
        ;; ("my[34]server" (".*" (("private" "To:.*Simon")
        ;; ("junk"    my-junk-func))))
        ))

(setq gnus-parameters
           '((".*"
              ;(gnus-show-threads t)
              ;(gnus-use-scoring nil)
              ;(gnus-summary-line-format
              ; "%U%R%z%I%(%[%d:%ub%-23,23f%]%) %s\n")
              (gcc-self . t)
              (display . all)
              )
     
             ;("^nnimap:\\(foo.bar\\)$"
             ; (to-group . "\\1"))
     
            ; ("mail\\.me"
             ; (gnus-use-scoring  t))
     
  ;           ("list\\..*"
  ;            (total-expire . t)
  ;            (broken-reply-to . t))
))
 
;; para que el que revisa las palabras sea aspell

(setq ispell-program-name "aspell")

;; (add-hook 'message-send-hook 'ispell-message)

;; spell-checking on the fly
(add-hook 'message-mode-hook (lambda () (flyspell-mode 1)))
;; para usar topics y evitar ambiwedad de nombres
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; para separar los buffers
(gnus-add-configuration
 '(article
   (horizontal 1.0
	       (vertical 25
			 (group 1.0))
	       (vertical 1.0
			 (summary 0.25 point)
			 (article 1.0)))))
(gnus-add-configuration
 '(summary
   (horizontal 1.0
	       (vertical 25
			 (group 1.0))
	       (vertical 1.0
			 (summary 1.0 point)))))


;; para mostrar los headers que yo quiero...
(setq gnus-visible-headers 
"^\\(From:\\|To:\\|Cc:\\|Bcc:\\|Subject:\\|Date:\\|Followup-To:\\|X-Newsreader:\\|User-Agent:\\|X-Mailer:\\)")


;; para unir los mensajes por thread y por subject, algunos 
;; clientes dividen/separan los hilos o crean nuevos

(setq gnus-summary-thread-gathering-function
      'gnus-gather-threads-by-references)

;; If you're spooling in overlapping mbox folders, you probably want
;; to delete all messages with duplicate message IDs.

(setq nnmail-treat-duplicates 'delete)



;; Buttonize the different parts, please
(setq gnus-buttonized-mime-types '("multipart/encrypted" 
                                   "multipart/signed"
                                   ""
                                   )
      )

;; But keep buttons for multiple parts
(setq gnus-inhibit-mime-unbuttonizing t)


                                        ;(gnus-group-split-setup auto-update catch-all)
                                        ;(gnus-group-split-setup t)
;; (setq gnus-group-line-format "%M\%S\%p\%5y: %g\n")


;; correct count and unread messages
;; from http://www.emacswiki.org/emacs/GnusNiftyTricks#toc9
;; (defun gnus-user-format-function-t (dummy)
;;   (case (car gnus-tmp-method)
;;     (nnimap
;;      (let ((count (nnimap-request-message-count gnus-tmp-qualified-group)))
;;        (if count
;;            (format "%d" count)
;;          "?")))
;;     (t
;;      gnus-tmp-number-total)))

;; (defvar nnimap-message-count-cache-alist nil)

;; (defun nnimap-message-count-cache-clear nil
;;   (setq nnimap-message-count-cache-alist nil))

;; (defun nnimap-message-count-cache-get (mbox &optional server)
;;   (when (nnimap-possibly-change-server server)
;;     (cadr (assoc (concat nnimap-current-server ":" mbox)
;;                  nnimap-message-count-cache-alist))))

;; (defun nnimap-message-count-cache-set (mbox count &optional server)
;;   (when (nnimap-possibly-change-server server)
;;     (push (list (concat nnimap-current-server ":" mbox)
;;                 count) nnimap-message-count-cache-alist))
;;   count)



;; (defun nnimap-request-message-count (mbox &optional server)
;;   (let ((count (or (nnimap-message-count-cache-get mbox server)
;;                    (and (nnimap-possibly-change-server server)
;;                         (progn
;;                           (message "Requesting message count for %s..."
;;                                    mbox)
;;                           (prog1
;;                               (imap-mailbox-status
;;                                mbox 'messages nnimap-server-buffer)
;;                             (message "Requesting message count for %s...done"
;;                                      mbox)))))))
;;     (when count
;;       (nnimap-message-count-cache-set mbox count server))
;;     count))

;; (add-hook 'gnus-after-getting-new-news-hook 'nnimap-message-count-cache-clear)

;(load-file "~/.emacs.d/dim-gnus-imap-count.el")
;(require 'dim-gnus-imap-count)

(setq gnus-group-mode-line-format "Gnus: %%b"
      ;; gnus-group-line-format "%M%S%p%P%5y:%B%(%g%)%O\n" ;; %uy is for unread messages, function declared above, %ut is for total count
      ;; gnus-group-line-format "%M%S%p%P%U:%uy%ut:%B%G\n"
      gnus-group-line-format "%M%S%p%P%5y:%B%(%G%)\n"
      gnus-topic-line-format "%i[ %{%(%n%)%} (%g/%A) ]%v\n")


(setq-default
 gnus-summary-line-format "%U%R%z %([%-5,5k %&user-date; %-20,20f] %B%-80,80s%)\n"
 gnus-user-date-format-alist '((t . "<%a %d %b %Y, %H:%M>"))
                                        ;gnus-summary-line-format "%U%R%d %-5,5L %-20,20n %B%-80,80S\n"
 gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references
                                        ;gnus-thread-sort-functions '(gnus-thread-sort-by-date)
 gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-date)
                                        ; gnus-sum-thread-tree-false-root ""
                                        ; gnus-sum-thread-tree-indent " "
                                        ; gnus-sum-thread-tree-leaf-with-other "├► "
                                        ; gnus-sum-thread-tree-root ""
                                        ; gnus-sum-thread-tree-single-leaf "╰► "
                                        ; gnus-sum-thread-tree-vertical "│"

                                        ; gnus-sum-thread-tree-false-root      "> "
                                        ; gnus-sum-thread-tree-single-indent   ""
                                        ; gnus-sum-thread-tree-root            "> "
                                        ; gnus-sum-thread-tree-vertical        "| "
                                        ; gnus-sum-thread-tree-leaf-with-other "+-> "
                                        ; gnus-sum-thread-tree-single-leaf "  \\-> "
                                        ; gnus-sum-thread-tree-indent          "  "

 )


(require 'bbdb)
(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
(require 'bbdb-hooks)
(bbdb-initialize 'gnus 'message)
(bbdb-insinuate-gnus)
(bbdb-insinuate-message)
;; (setq bbdb-use-pop-up t)
;;No popup-buffers
(setq bbdb-use-pop-up nil)
;; Automatically add bbdb entries.
(setq bbdb/mail-auto-create-p 'bbdb-prune-not-to-me)
(setq bbdb/news-auto-create-p 'bbdb-prune-not-to-me)
(defun bbdb-prune-not-to-me ()
  "defun called when bbdb is trying to automatically create a record. Filters out
anything not actually adressed to me then passes control to 'bbdb-ignore-some-messages-hook'.
Also filters out anything that is precedense 'junk' or 'bulk' This code is from
Ronan Waide < waider @ waider . ie >."
  (let ((case-fold-search t)
        (done nil)
        (b (current-buffer))
        (marker (bbdb-header-start))
        field regexp fieldval)
    (set-buffer (marker-buffer marker))
    (save-excursion
      ;; Hey ho. The buffer we're in is the mail file, narrowed to the
      ;; current message.
      (let (to cc precedence)
        (goto-char marker)
        (setq to (bbdb-extract-field-value "To"))
        (goto-char marker)
        (setq cc (bbdb-extract-field-value "Cc"))
        (goto-char marker)
        (setq precedence (bbdb-extract-field-value "Precedence"))
        ;; Here's where you put your email information.
        ;; Basically, you just add all the regexps you want for
        ;; both the 'to' field and the 'cc' field.
;; (if (and (not (string-match "noufal@\\|linux.*@" (or to "")))
;; (not (string-match "noufal@\\|linux.*@" (or cc ""))))
            (progn
              (message "BBDB unfiling; message to: %s cc: %s"
                       (or to "noone") (or cc "noone"))
              ;; Return nil so that the record isn't added.
              nil)

          (if (string-match "junk" (or precedence ""))
              (progn
                (message "precedence set to junk, bbdb ignoring.")
                nil)

            ;; Otherwise add, subject to filtering
            (bbdb-ignore-some-messages-hook))))));)



(add-hook 'message-mode-hook
          (function (lambda() 
                      (local-set-key (kbd "<tab>") 'bbdb-complete-name)
                      )))

(setq w3m-toggle-inline-images t)


;; para enviar correo con msmtp:

;; with Emacs 23.1, you have to set this explicitly (in MS Windows)
;; otherwise it tries to send through OS associated mail client
(setq message-send-mail-function 'message-send-mail-with-sendmail)
;; we substitute sendmail with msmtp
(setq sendmail-program "/usr/bin/msmtp")



;; para decirle a msmtp con qué cuenta enviar correo:
(setq gnus-posting-styles
      '((".*gnuchile.*" 
         (address "tsolar@fundaciongnuchile.cl")
         (eval (setq message-sendmail-extra-arguments '("-a" "gnuchile")))
         (user-mail-address "tsolar@fundaciongnuchile.cl")
         )
        (".*fsfla.*" 
         (address "tsolar@fsfla.org")
         (eval (setq message-sendmail-extra-arguments '("-a" "fsfla")))
         (user-mail-address "tsolar@fsfla.org")
         )
        (".*lavabit.*" 
         (address "tsolar@lavabit.com")
         (eval (setq message-sendmail-extra-arguments '("-a" "lavabit")))
         (user-mail-address "tsolar@lavabit.com")
         )
        ;; ("" 
        ;;  (address "tsolar@fundaciongnuchile.cl")
        ;;  (eval (setq message-sendmail-extra-arguments '("-a" "gnuchile")))
        ;;  (user-mail-address "tsolar@fundaciongnuchile.cl")
        ;;  )
        )
      )

;; Automatically sign when sending mails
;; (add-hook 'message-send-hook 'mml-secure-sign-pgpmime)
;; only sign
(add-hook 'gnus-message-setup-hook 'mml-secure-sign-pgpmime)

;;; Crypt-foo
;; (this would include pgg, if i wouldn't use the defaults)
(setq gnus-message-replysign t
      ;; gnus-message-replyencrypt t
      mm-verify-option 'always
      ;; mm-decrypt-option 'always
      )

;; reviso el correo cada cierto tiempo
(require 'gnus-demon)
(setq gnus-use-demon t)
(gnus-demon-add-handler 'gnus-group-get-new-news 5 2)
(gnus-demon-init)

;; desktop notifications!
(require 'gnus-desktop-notify)
(gnus-desktop-notify-mode)
(gnus-demon-add-scanmail)

;;highline mode for gnus
(add-hook 'gnus-summary-mode-hook 'my-setup-hl-line)
(add-hook 'gnus-group-mode-hook 'my-setup-hl-line)

(defun my-setup-hl-line ()
  (hl-line-mode 1)
  (setq cursor-type nil) ;; Comment this out, if you want the cursor to stay visible.
  )
