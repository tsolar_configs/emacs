(add-to-list 'load-path "~/.emacs.d")

;(setq initial-frame-alist '((width . 87) (height . 22)))
;(setq default-file-name-coding-system 'undecided)

;; (setq debug-on-error nil)

;; new php-mode :)
;; http://git.piprime.fr/
;; git clone git://piprime.fr/emacs/pi-php-mode.git
(add-to-list 'load-path "~/.emacs.d/pi-php-mode")
;; (load  "~/.emacs.d/pi-php-mode/pi-php-mode.el")
(require 'pi-php-mode)

(add-hook 'php-mode-hook
          '(lambda () (define-abbrev php-mode-abbrev-table "ex" "extends")))

(add-hook 'php-hook (lambda()
                         (condition-case nil
                             (imenu-add-menubar-index)
                           (error nil)
			   )
			 )
	  )

;; svn checkout http://autopair.googlecode.com/svn/trunk/ autopair
(add-to-list 'load-path "~/.emacs.d/autopair") ;; comment if autopair.el is in standard load path 
(require 'autopair)
(autopair-global-mode) ;; enable autopair in all buffers

;; muestra la región entre paréntesis
(setq show-paren-mode t) 
(setq show-paren-style 'expression) 

(transient-mark-mode 1) ; highlight text selection
(delete-selection-mode 1) ; delete seleted text when typing

;; turn on highlighting current line
(global-hl-line-mode 1)

(defun volatile-kill-buffer ()
  "Kill current buffer unconditionally."
  (interactive)
  (let ((buffer-modified-p nil))
    (kill-buffer (current-buffer))))

;; And bind this to the same key as the original kill-buffer:

(global-set-key (kbd "C-x k") 'volatile-kill-buffer)

(fset 'yes-or-no-p 'y-or-n-p)

;; Only spaces, please!
(setq-default indent-tabs-mode nil)
(setq tab-width 4)
(setq-default c-basic-offset 4)

(put 'downcase-region 'disabled nil)

(put 'upcase-region 'disabled nil)

;; ftp 
(setq tramp-default-method "ftp")


;; remember and org mode

(require 'remember)

(setq remember-annotation-functions '(org-remember-annotation))
(setq remember-handler-functions '(org-remember-handler))
(add-hook 'remember-mode-hook 'org-remember-apply-template)


;; The following lines are always needed.  Choose your own keys.
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
;; (global-set-key "\C-cl" 'org-store-link)
;; (global-set-key "\C-ca" 'org-agenda)
;; (global-set-key "\C-cb" 'org-iswitchb)

(setq org-log-done 'note)

;; todo states
;; #+TODO: TODO(t) | DONE(d)
;; #+TODO: REPORT(r) BUG(b) KNOWNCAUSE(k) | FIXED(f)
;; #+TODO: | CANCELED(c)

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(appmenu-mode t)
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(display-battery-mode t)
 '(display-time-mode t)
 '(ecb-options-version "2.40")
 '(elscreen-display-tab t)
 '(emms-player-list (quote (emms-player-mpd emms-player-mpg321 emms-player-ogg123 emms-player-vlc)))
 '(erc-auto-query (quote window-noselect))
 '(erc-autoaway-mode t)
 '(erc-away-nickname nil)
 '(erc-join-buffer (quote window-noselect))
 '(erc-modules (quote (completion list menu scrolltobottom autojoin button dcc fill irccontrols match move-to-prompt netsplit networks noncommands readonly ring stamp spelling track)))
 '(erc-prompt ">")
 '(erc-public-away-p t)
 '(erc-speedbar-sort-users-type (quote alphabetical))
 '(erc-user-full-name "Tomás Solar")
 '(garak-alert-methods (quote (:notify)))
 '(garak-alert-when (quote (:new :hidden :chat)))
 '(garak-hide-offline-buddies t)
 '(gnus-desktop-notify-mode t nil (gnus))
 '(identica-display-success-messages t)
 '(identica-soft-wrap-status t)
 '(inhibit-startup-screen t)
 '(jabber-alert-message-hooks (quote (jabber-message-awesome jabber-message-echo jabber-message-scroll)))
 '(jabber-default-status "http://tsolar.cl http://identi.ca/tsolar")
 '(jabber-roster-sort-functions (quote (jabber-roster-sort-by-status jabber-roster-sort-by-displayname jabber-roster-sort-by-group)))
 '(jabber-show-offline-contacts nil)
 '(lui-flyspell-p t)
 '(lui-time-stamp-only-when-changed-p t)
 '(lui-time-stamp-position (quote left-margin))
 '(mml-default-sign-method "pgpmime")
 '(mumamo-margin-info-global-mode t)
 '(mumamo-submode-indent-offset 4)
 '(nxhtml-autoload-web nil)
 '(org-agenda-files nil)
 '(pop3-leave-mail-on-server t)
 '(pop3-maildrop "tsolar@gnuchile.cl")
 '(pop3-mailhost "mail.gnuchile.cl")
 '(rainbow-x-colors-major-mode-list (quote (emacs-lisp-mode lisp-interaction-mode c-mode c++-mode java-mode lua-mode html-helper-mode php-mode css-mode lisp-mode)))
 '(save-place t nil (saveplace))
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(sml-modeline-mode t)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(user-mail-address "tsolar@fundaciongnuchile.cl")
 '(w3m-default-display-inline-images t)
 '(wl-user-mail-address-list (quote ("tsolar@parabolagnulinux.org" "tsolar@lavabit.com"))))

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "grey15" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 95 :width normal :foundry "xos4" :family "Inconsolata"))))
 '(cursor ((t (:background "yellow" :foreground "black"))))
 '(erc-notice-face ((t (:foreground "SlateBlue" :weight bold))))
 '(fixed-pitch ((t (:family "Inconsolata"))))
 '(highlight ((t (:background "deep sky blue" :foreground "black"))))
 '(hl-line ((default (:background "grey25")) (nil nil)))
 '(mode-line ((t (:background "forest green" :foreground "grey90" :box (:line-width -1 :style released-button) :family "Inconsolata"))))
 '(mode-line-inactive ((default (:inherit mode-line :family "Inconsolata")) (((class color) (min-colors 88) (background dark)) (:background "grey30" :foreground "grey80" :box (:line-width -1 :color "grey40") :weight semi-light))))
 '(mumamo-background-chunk-major ((default nil) (nil nil)))
 '(mumamo-background-chunk-submode1 ((((class color) (min-colors 88) (background dark)) nil)))
 '(mumamo-background-chunk-submode2 ((((class color) (min-colors 88) (background dark)) nil)))
 '(mumamo-background-chunk-submode3 ((((class color) (min-colors 88) (background dark)) nil)))
 '(mumamo-background-chunk-submode4 ((((class color) (min-colors 88) (background dark)) nil)))
 '(region ((((class color) (min-colors 88) (background dark)) (:background "#535d6c"))))
 '(semantic-highlight-edits-face ((((class color) (background dark)) (:background "gray50"))))
 '(tooltip ((((class color)) (:inherit variable-pitch :background "lightyellow" :foreground "black" :family "Inconsolata"))))
 '(twittering-uri-face ((t (:foreground "#95E8EC"))))
 '(twittering-username-face ((t (:foreground "#0099B9")))))

;; calendar localization
(setq calendar-day-name-array ["lunes" "martes" "miércoles" "jueves" "viernes" "sábado" "domingo"]
      calendar-month-name-array ["enero" "febrero" "marzo" "abril" "mayo"
                                 "junio" "julio" "agosto" "septiembre"
                                 "octubre" "noviembre" "diciembre"])


;;browser

(require 'w3m-load)
(require 'w3m)

(setq browse-url-generic-program (executable-find "icecat")
      browse-url-browser-function 'browse-url-generic)
;(setq browse-url-browser-function 'browse-url-generic
;      browse-url-generic-program "/usr/bin/conkeror")

(defun choose-browser (url &rest args)
  (interactive "sURL: ")
  (if (y-or-n-p "Use external browser? ")
      (browse-url-generic url)
    (w3m-browse-url url)))

(setq browse-url-browser-function 'choose-browser)

;;; algunos modos
(tool-bar-mode 0)
(iswitchb-mode 1)
(flyspell-mode 1)
(ido-mode 1)
(hl-line-mode t)
;(icomplete-mode 1)
(setq c-default-style "bsd") 
(setq-default scroll-conservatively 1)


;; ;; atajo para comentar
(global-set-key [(control shift c)] 'comment-or-uncomment-region+)

(global-set-key [(shift f1)] 'buffer-menu)



;;tabbar
;(require 'tabbar)
;(tabbar-mode 1)

;;;;;;;;;;;;;;;;;;;;;;
;(setq tabbar-buffer-groups-function
;      (lambda ()
;        (list "All Buffers")))

;(setq tabbar-buffer-list-function
;      (lambda ()
;        (remove-if
;         (lambda(buffer)
;           (find (aref (buffer-name buffer) 0) " *"))
;         (buffer-list))))

;; C-S-<tab> ;; C-S-<win>-<tab>
;(global-set-key (kbd "<C-S-iso-lefttab>") 'tabbar-forward-tab)
;(global-set-key (kbd "<C-S-s-iso-lefttab>") 'tabbar-backward-tab)
;; C-x C-<left> ;; C-x C-<right>
;(global-set-key (kbd "C-x C-<right>") 'tabbar-forward-group)
;(global-set-key (kbd "C-x C-<left>") 'tabbar-backward-group)
;;;;;;tabbar fin

;;;anything
;(add-to-list 'load-path "~/.emacs.d/anything")
;(require 'anything-config)

;;;autocomplete
(add-to-list 'load-path "~/.emacs.d/auto-complete")

;; mercurial stuff
(load "~/.emacs.d/mercurial.el")
(add-to-list 'load-path "~/.emacs.d/ahg")
(require 'ahg)

;; magit - a git mode for emacs
(add-to-list 'load-path "~/.emacs.d/magit")
(require 'magit)

;; git-emacs, git-blame...
;; git clone git://github.com/tsgates/git-emacs.git
(add-to-list 'load-path "~/.emacs.d/git-emacs")
;;============================================================
;; git-emacs mode
;;------------------------------------------------------------
;; (eval-when-compile (require 'soo-load))
(setq git-state-modeline-decoration 'git-state-decoration-large-dot)
(require 'git-emacs-autoloads)
;;------------------------------------------------------------

(require 'git-blame)
(autoload 'git-blame-mode "git-blame"
  "Minor mode for incremental blame for Git." t)
;; -----------------------------------------------------------

;;completamiento en un menú...
(add-to-list 'load-path "~/.emacs.d/completion-ui")
(require 'completion-ui)

;;para identica se necesita oauth
(add-to-list 'load-path "~/.emacs.d/emacs-oauth")

;;; Identi.ca mode
(add-to-list 'load-path "~/.emacs.d/identica-mode")
;(setq statusnet-server "www.gnewbook.org")
;(add-to-list 'load-path "~/Descargas/identica-mode/identica-mode")
(require 'identica-mode)
(load "~/.emacs.d/identica-auth")
(setq identica-auth-mode "oauth")

;;;Twittering mode
(add-to-list 'load-path "~/.emacs.d/twittering-mode") ;; if you need
(require 'twittering-mode)
(setq twittering-auth-method 'oauth)
;; To avoid having to authorize twittering-mode everytime you run it, add this to your `.emacs`:
(setq twittering-use-master-password t)

;; This has to be put in twitter-auth file
;; (defun
  ;; epa-passphrase-callback-function(context key-id handback) "YOUR PASSPHRASE") ;; in this case, "YOUR PASSPHRASE" is actually your master passowrd

;; so then I load the credentials :)
(load "~/.emacs.d/twitter-auth")

(twittering-mode )

(identica-mode )

(set-face-foreground 'identica-username-face "tomato");"#8F0000")
(set-face-foreground 'identica-uri-face "#9BB43E");"#87B4C8")
;(set-face-foreground 'identica-reply-face "DodgerBlue3");"#9BB43E")
(set-face-background 'identica-reply-face "RoyalBlue4")

(set-face-underline-p 'identica-username-face nil)
(set-face-underline-p 'identica-uri-face nil)
(set-face-underline-p 'identica-reply-face nil)

(set-face-foreground 'twittering-username-face "#0099B9")
(set-face-foreground 'twittering-uri-face "#95E8EC")

;;yasnipets
;(add-to-list 'load-path "/usr/share/emacs/site-lisp/yas")
;(require 'yasnippet) ;; not yasnippet-bundle
;(yas/initialize)
;(yas/load-directory "/usr/share/emacs/site-lisp/yas/snippets")
                

;; flymake
;; (require 'flymake)
;; (defun my-flymake-show-next-error()
;;   (interactive)
;;   (flymake-goto-next-error)
;;   (flymake-display-err-menu-for-current-line)
;;   )

;; (local-set-key "\C-c\C-v" 'my-flymake-show-next-error)


;;php D:
;(autoload 'php-mode "php-mode" "Php mode." t)
;(setq auto-mode-alist (append '(("/*.\.php[345]?$" . php-mode)) auto-mode-alist))
;(add-hook ‘php-mode-hook (lambda() (flymake-mode 1)))

;; (require 'flymake)
(add-hook 'find-file-hook 'flymake-mode)

;;; lua mode;
(setq auto-mode-alist (cons '("\.lua$" . lua-mode) auto-mode-alist))
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(eval-after-load "lua-mode"
  '(add-hook 'lua-mode-hook 'rainbow-mode))
;; flymake lua
(require 'flymake-lua)
(add-hook 'lua-mode-hook 'flymake-lua-load)

(eval-after-load "css-mode"
  '(add-hook 'css-mode-hook 'rainbow-mode))

;;Here are instructions how to make flymake work with HTML:

;; (defun flymake-html-init ()
;;   (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                      'flymake-create-temp-inplace))
;;          (local-file (file-relative-name
;;                       temp-file
;;                       (file-name-directory buffer-file-name))))
;;     (list "tidy" (list local-file))))

;; (add-to-list 'flymake-allowed-file-name-masks
;;              '("\\.html$\\|\\.ctp" flymake-html-init))

;; (add-to-list 'flymake-err-line-patterns
;;              '("line \\([0-9]+\\) column \\([0-9]+\\) - \\(Warning\\|Error\\): \\(.*\\)"
;;                nil 1 2 4))



;; flymake for php
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;php-mode
;(autoload 'php-mode "php-mode" "Php mode." t)
;(setq auto-mode-alist (append '(("/*.\.php[345]?$" . php-mode)) auto-mode-alist))

;; Flymake PHP Extension
;; (require 'flymake)
;; (unless (fboundp 'flymake-php-init)
;;   (defun flymake-php-init ()
;;     (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                        'flymake-create-temp-inplace))
;;            (local-file (file-relative-name
;;                         temp-file
;;                         (file-name-directory buffer-file-name))))
;;       (list "php" (list "-f" local-file "-l")))))

;; (let ((php-ext-re "\\.php[345]?\\'")
;;       (php-error-re
;;        "\\(?:Parse\\|Fatal\\) error: \\(.*\\) in \\(.*\\) on line \\([0-9]+\\)"))
;;   (unless (assoc php-ext-re flymake-allowed-file-name-masks)
;;     (add-to-list 'flymake-allowed-file-name-masks
;;                  (list php-ext-re
;;                        'flymake-php-init
;;                        'flymake-simple-cleanup
;;                        'flymake-get-real-file-name))
;;     (add-to-list 'compilation-error-regexp-alist-alist
;;                  (list 'compilation-php
;;                        php-error-re  2 3 nil nil))
;;     (add-to-list 'compilation-error-regexp-alist 'compilation-php)
;;     (add-to-list 'flymake-err-line-patterns
;;                  (list php-error-re 2 3 nil 1))))

;; (add-hook 'php-mode-hook (lambda () (flymake-mode t)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; add these lines to your .emacs file:
;; -------------- .emacs -----------------------------
;(add-hook 'php-mode-hook
;          (lambda ()
;            (require 'php-completion)
;            (php-completion-mode t)
;            ;(define-key php-mode-map (kbd "C-o") 'phpcmp-complete)
;            ))
;; ---------------------------------------------------

;; Cooperation with auto-complete.el:
;;
;; add these lines to your .emacs file:
(add-hook  'php-mode-hook
           (lambda ()
             (when (require 'auto-complete nil t)
               (make-variable-buffer-local 'ac-sources)
               (add-to-list 'ac-sources 'ac-source-php-completion)
               ;; if you like patial match,
               ;; use `ac-source-php-completion-patial' instead of `ac-source-php-completion'.
               (add-to-list 'ac-sources 'ac-source-php-completion-patial)
               (auto-complete-mode t))))


;;;php...
;(require 'php-mode)
;(autoload 'php-mode "php-mode" "Major mode for editing php code." t)
;(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
;(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))

;(require 'flymake)

;; (defun flymake-php-init ()
;;   "Use php to check the syntax of the current file."
;;   (let* ((temp (flymake-init-create-temp-buffer-copy 'flymake-create-temp-inplace))
;; 	 (local (file-relative-name temp (file-name-directory buffer-file-name))))
;;     (list "php" (list "-f" local "-l"))))

;; (add-to-list 'flymake-err-line-patterns
;;   '("\\(Parse\\|Fatal\\) error: +\\(.*?\\) in \\(.*?\\) on line \\([0-9]+\\)$" 3 4 nil 2))

;; (add-to-list 'flymake-allowed-file-name-masks '("\\.php$" flymake-php-init))

;; ;; Drupal-type extensions
;; (add-to-list 'flymake-allowed-file-name-masks '("\\.module$" flymake-php-init))
;; (add-to-list 'flymake-allowed-file-name-masks '("\\.install$" flymake-php-init))
;; (add-to-list 'flymake-allowed-file-name-masks '("\\.inc$" flymake-php-init))
;; (add-to-list 'flymake-allowed-file-name-masks '("\\.engine$" flymake-php-init))

;; (add-hook 'php-mode-hook (lambda () (flymake-mode 1)))
;; ;(define-key php-mode-map '[M-S-up] 'flymake-goto-prev-error)
;; ;(define-key php-mode-map '[M-S-down] 'flymake-goto-next-error)
;;;;;;;;;;;;;;;;end flymake php

;;yaml
(require 'yaml-mode)
(setq auto-mode-alist
      (append '(("\.yml\'" . yaml-mode))
              auto-mode-alist))


;; Load CEDET
;(load-file "/usr/share/emacs/site-lisp/cedet/common/cedet.el")
;(load-file "~/.emacs.d/cedet-1.0pre7/common/cedet.el")
;;;;;;;;;;;;;;;;;;;;
;(require 'cedet)
;(global-ede-mode t)
;(require 'semantic-ia)
;(semantic-load-enable-excessive-code-helpers)
;(semantic-load-enable-code-helpers)

;; (require 'cedet)
(global-ede-mode t)
;;(semantic-load-enable-code-helpers)

;; smart completion - disabled
;(require 'semantic-ia) ;; nil t

;; additional options
;(require 'semantic-sb)
;(require 'semanticdb)
;(require 'semantic-fold)

;(global-semanticdb-minor-mode 1)
;(require 'semantic/analyze)
(provide 'semantic-analyze)
(provide 'semantic-ctxt)
(provide 'semanticdb)
(provide 'semanticdb-find)
(provide 'semanticdb-mode)
(provide 'semantic-load)
(global-semanticdb-minor-mode 1)
;(add-to-list 'load-path "/usr/share/emacs/site-lisp/ecb")
(add-to-list 'load-path "~/.emacs.d/ecb-snap")
(require 'ecb)


;; * This turns on which-func support (Plus all other code helpers), remove if not X server
;; (semantic-load-enable-minimum-features)
;(semantic-load-enable-code-helpers) ;; It is nice but i really dislike completion splitting window in 2
;(semantic-load-enable-excessive-code-helpers)
;; (semantic-load-enable-gaudy-code-helpers)
(setq senator-minor-mode-name "SN")

;; Be careful to add lines for semantic-idle because weird tooltip box appears again :(
(global-semantic-idle-summary-mode 1) ;Display a tag summary of the lexical token under the cursor.
;(global-semantic-show-unmatched-syntax-mode 1)

;; Add menu to navigate faster between tokens
(add-hook 'semantic-init-hooks (lambda ()
                                 (imenu-add-to-menubar "TAGS")))

;; Enable semantic cache, search functions only first time
(global-semanticdb-minor-mode 1)

(defun semanticdb-cache-directory-p(directory)
  (cond
   ((search "/ArraySaver/" directory) nil)
   ((search "@@" directory) t)
   (t nil)))

(add-hook 'semanticdb-project-predicate-functions 'semanticdb-cache-directory-p)

(setq semanticdb-default-save-directory "~/.emacs.d/semantic")

;; enable ctags for some languages:
;;  Unix Shell, Perl, Pascal, Tcl, Fortran, Asm
;(semantic-load-enable-primary-exuberent-ctags-support)

;; if you want to enable support for gnu global
;; (require 'semanticdb-global)
;(semanticdb-enable-gnu-global-databases 'c-mode)
;(semanticdb-enable-gnu-global-databases 'c++-mode)
;; (semanticdb-enable-gnu-global-databases 'php-mode)

;;fin cedet;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;tidy 
(autoload 'tidy-buffer "tidy" "Run Tidy HTML parser on current buffer" t)
(autoload 'tidy-parse-config-file "tidy" "Parse the `tidy-config-file'" t)
(autoload 'tidy-save-settings "tidy" "Save settings to `tidy-config-file'" t)
(autoload 'tidy-build-menu  "tidy" "Install an options menu for HTML Tidy." t)
;;
;; If you use html-mode to edit HTML files then add something like
;; this as well

(defun my-html-mode-hook () "Customize my html-mode."
  (tidy-build-menu html-mode-map)
  (local-set-key [(control c) (control c)] 'tidy-buffer)
  (setq sgml-validate-command "tidy"))

(add-hook 'html-mode-hook 'my-html-mode-hook)

;; This will set up a "tidy" menu in the menu bar and bind the key
;; sequence "C-c C-c" to `tidy-buffer' in html-mode (normally bound to
;; `validate-buffer').
;;
;; For other modes (like html-helper-mode) simple change the variables
;; `html-mode-hook' and `html-mode-map' to whatever is appropriate e.g.

; (defun my-html-mode-hook () "Customize my html-helper-mode."
;   (tidy-build-menu html-helper-mode-map)
;   (local-set-key [(control c) (control c)] 'tidy-buffer)
;   (setq sgml-validate-command "tidy"))

; (add-hook 'html-helper-mode-hook 'my-html-mode-hook)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(add-to-list 'load-path "~/.emacs.d/completion-ui")
;(require 'completion-ui)

;(require 'ecb)

;(require 'html-helper-mode)

;; (eval-after-load
;;     "nxml-mode"
;;   '(progn
;;      (setq html-helper-htmldtd-version "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n")

;;      (setq html-helper-new-buffer-template
;;                 '(html-helper-htmldtd-version
;;                       "<html>\n"
;;                       "<head>\n{Base::header()}\n</head>\n"
;;                       "\n<body>\n"
;; (setq html-helper-basic-offset 4)
;; (setq html-helper-item-continue-indent 4)

;(require 'javascript-mode)
;(require 'js2-mode)
;(require 'css-mode)

;;(setq sgml-warn-about-undefined-entities nil)
;;************************************************************
;; configure HTML editing
;;************************************************************
;;
;(require 'php-mode)
;;
;; configure css-mode
;(autoload 'css-mode "css-mode")
;(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))
;(setq cssm-indent-function 'cssm-c-style-indenter)
;(setq cssm-indent-level '0)

;; (add-hook 'php-mode-user-hook 'turn-on-font-lock)

;; (require 'mmm-mode)
;; (setq mmm-global-mode 'maybe)

;; ;; set up an mmm group for fancy html editing
;; (mmm-add-group
;;  'fancy-html
;;  '(
;;    (html-php-embedded
;;         :submode php-mode
;;         :face mmm-output-submode-face
;;         :front "<[?]php"
;;         :back "[?]>")
;;    (html-css-embedded
;;         :submode css-mode
;;         :face mmm-declaration-submode-face
;;         :front "<style\[^>\]*>"
;;         :back "</style>")
;;    (html-css-attribute
;;         :submode css-mode
;;         :face mmm-declaration-submode-face
;;         :front "\\bstyle=\\s-*\""
;;         :back "\"")
;;    (html-javascript-embedded
;;         :submode javascript-mode
;;         :face mmm-code-submode-face
;;         :front "<script type=\"text/javascript\">"
;;         :back "</script>")
;;    (html-javascript-attribute
;;         :submode javascript-mode
;;         :face mmm-code-submode-face
;;         :front "\\bon\\w+=\\s-*\""
;;         :back "\"")
;;    )
;;  )
;; (mmm-add-group
;;  'fancy-php
;;  '(
;;    (php-sql-embedded
;;         :submode sql-mode
;;         :face mmm-code-submode-face
;;         :front "sdfsql = \""
;;         :back "\";")
;;    )
;;  )

;; ;(mmm-add-group
;; ; 'fancy-django
;; ; '(
;; ;   (my-django-embedded
;; ;    :submode python-mode
;; ;    :face mmm-declaration-submode-face
;; ;    :front "{%"
;; ;    :back "%}"
;; ;    :include-front t
;; ;    :include-back t)
;; ;   (my-django-var
;; ;    :submode python
;; ;    :face mmm-output-submode-face
;; ;    :front "{{"
;; ;    :back "}}"
;; ;    :include-front t
;; ;    :include-back t)))


;; ;; What files to invoke the new html-mode for?
;; (add-to-list 'auto-mode-alist '("\\.inc\\'" . html-mode))
;; (add-to-list 'auto-mode-alist '("\\.phtml\\'" . html-mode))
;; (add-to-list 'auto-mode-alist '("\\.php[34]?\\'" . html-helper-mode))
;; (add-to-list 'auto-mode-alist '("\\.[sji]?html?\\'" . nxml-mode))
;; (add-to-list 'auto-mode-alist '("\\.jsp\\'" . html-mode))
;; ; (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . html-helper-mode))
;; (add-to-list 'auto-mode-alist '("\\.tt\\'" . nxml-mode )) ;; Catalyst templates

;; ;; What features should be turned on in this html-mode?
;; ;; (add-to-list 'mmm-mode-ext-classes-alist '(html-helper-mode nil html-js))
;; (add-to-list 'mmm-mode-ext-classes-alist '(nxml-mode nil fancy-html))
;; (add-to-list 'mmm-mode-ext-classes-alist '(nxml-mode nil embedded-css))
;; (add-to-list 'mmm-mode-ext-classes-alist '(nxml-mode nil fancy-html))
;; ;; (add-to-list 'mmm-mode-ext-classes-alist '(nxml-mode nil fancy-django))



;;  (add-to-list 'mmm-mode-ext-classes-alist '(php-mode nil fancy-php))

;; ;; Not exactly related to editing HTML: enable editing help with mouse-3 in all sgml files
;; ;(defun go-bind-markup-menu-to-mouse3 ()
;; ;  (define-key sgml-mode-map [(down-mouse-3)] 'sgml-tags-menu))

;; ;(add-hook 'sgml-mode-hook 'go-bind-markup-menu-to-mouse3)
;; ;(set-face-background 'mmm-default-submode-face nil)

;; ;(setq mmm-submode-decoration-level 0)

;; ;(add-hook 'nxml-mode
;; ;          '(when (string-match "\\.\\(x?html\\|php[34]?\\)$"
;; ;                              (file-name-sans-versions (buffer-file-name)))
;; ;            (my-xhtml-extras)))

;; ;(defun my-xhtml-extras ()
;; ;  (make-local-variable 'outline-regexp)
;; ;  (setq outline-regexp "\\s *<\\([h][1-6]\\|html\\|body\\|head\\)\\b")
;; ;  (make-local-variable 'outline-level)
;; ;  (setq outline-level 'my-xhtml-outline-level)
;; ;  (outline-minor-mode 1)
;; ;  (hs-minor-mode 1))

;; ;(defun my-xhtml-outline-level ()
;; ;  (save-excursion (re-search-forward html-outline-level))
;; ;  (let ((tag (buffer-substring (match-beginning 1) (match-end 1))))
;; ;    (if (eq (length tag) 2)
;; ;        (- (aref tag 1) ?0)
;; ;      0)))

;; ;(add-to-list 'hs-special-modes-alist
;; ;             '(nxml-mode
;; ;               "<!--\\|<[^/>]>\\|<[^/][^>]*[^/]>"
;; ;               ""
;; ;               "<!--" ;; won't work on its own; uses syntax table
;; ;               (lambda (arg) (my-nxml-forward-element))
;; ;               nil))

;; ;(defun my-nxml-forward-element ()
;; ;  (let ((nxml-sexp-element-flag))
;; ;    (setq nxml-sexp-element-flag (not (looking-at "<!--")))
;; ;    (unless (looking-at outline-regexp)
;; ;      (condition-case nil
;; ;          (nxml-forward-balanced-item 1)
;; ;        (error nil)))))



;; ;; Modificaciones SQL

;; ;(setq sql-indent-first-column-regexp (concat
;; ;                                                                          "^\\s-*"
;; ;                                                                          (regexp-opt
;; ;                                                                           '("select" "update" "insert" "delete" "create" "union" "intersect" "drop" "grant"
;; ;                                                                                 "from" "where" "into" "group" "having" "order" "set" "and" "or" "exists" "--"
;; ;                                                                                 "create" "returns" "begin" "end" "declare")
;; ;                                                                           t)
;; ;                                                                          "\\(\\b\\|\\s-\\)"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EMMS ------------------------
;;------------------------------
(add-to-list 'load-path "~/.emacs.d/emms/lisp")
(require 'emms-setup)
(require 'emms-streams)
(emms-all)
(emms-default-players)
(setq emms-player-mpg321-parameters '("-o" "alsa"))

(require 'emms-extension)
(add-to-list 'emms-info-functions 'emms-info-ogginfo)
(add-to-list 'emms-info-functions 'emms-info-mp3info)


;;fin emms

;;inicio mpd
(require 'emms-player-mpd)
(setq emms-player-mpd-server-name "localhost")
(setq emms-player-mpd-server-port "6600")
(add-to-list 'emms-info-functions 'emms-info-mpd)
(add-to-list 'emms-player-list 'emms-player-mpd)
(setq emms-player-mpd-music-directory "/home/tom/Music")
;;fin mpd

(setq emms-track-description-function
      (lambda (track)
        (let ((artist (emms-track-get track 'info-artist))
              (album  (emms-track-get track 'info-album))
              (number (emms-track-get track 'info-tracknumber))
              (title  (emms-track-get track 'info-title)))
          (if (and artist album title)
              (if number
                  (format "%s: %s - [%03d] %s" artist album (string-to-number number) title)
                (format "%s: %s - %s" artist album title))
            (emms-track-simple-description track)))))

;Additionally, we can change the face for the playlist to be a little cooler.

;(set-face-attribute 'emms-playlist-track-face    nil :font "DejaVu Sans-10")
(set-face-attribute 'emms-playlist-selected-face nil :background "White" :foreground "Firebrick")



;;;; inicio del widget ese...

(defun my-emms-ui-show-playlist ()
  (save-excursion
    (pop-to-buffer emms-playlist-buffer)
    (emms-playlist-mode-center-current))
  (message "Showing playlist"))

(defun my-emms-play-radio (channel)
  (my-emms-clear-playlist-if-any)
  ;; List of radio stations I listen to.
  (set-buffer (url-retrieve-synchronously "http://klibb.com/cgi-bin/wiki.pl/MathiasDahlRadio"))
  (goto-char (point-min))
  (let (urls)
    (while (search-forward-regexp "^<a href=\"\\([^\"]+\\)\">\\([^<]+\\)<" nil t)
      (push (cons (match-string 2) (match-string 1)) urls))
    (emms-play-streamlist (cdr (assoc channel urls)))))

(defun my-emms-clear-playlist-if-any ()
  (when emms-playlist-buffer
    ;(set-buffer ;save-excursion
      (set-buffer emms-playlist-buffer)
      (emms-playlist-clear)));)

(defun my-emms-play-all ()
  (interactive)
  (my-emms-clear-playlist-if-any)
  (emms-play-playlist "~/Music/playlists/todos.m3u"))

(defun my-emms-play-matching (pattern)
  (interactive "sPlay song matching: ")
  (my-emms-clear-playlist-if-any)
  (emms-play-find "~/Music/" pattern))

(defvar name-widget nil)

(defun my-emms-ui ()
  (interactive)
  (let (buf)
    (if (setq buf (get-buffer "*emms ui*"))
        (kill-buffer buf))
    (setq buf (get-buffer-create "*emms ui*"))
    (pop-to-buffer buf)
    (remove-overlays)
    (widget-insert "\n*** Kicki's music player ***\n\n")
    (setq name-widget
          (widget-create 'editable-field
                         :size 20
                         :format "Name: %v "
                         ""))
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (let ((pattern (widget-value name-widget)))
                               (my-emms-play-matching pattern)                               
                               (message "Playing songs matching \"%s\"" 
                                        pattern)
                               (my-emms-ui-show-playlist)))
                   "Play matching songs")
    (widget-insert "\n\n")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (my-emms-play-all)
                             (my-emms-ui-show-playlist)
                             (message "Playing all songs..."))
                   "Play all songs")
    (widget-insert "\n\nRadio stations: ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (my-emms-play-radio "P3")
                             (my-emms-ui-show-playlist))
                   "P3")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (my-emms-play-radio "P1")
                             (my-emms-ui-show-playlist))
                   "P1")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (my-emms-play-radio "Radioseven")
                             (my-emms-ui-show-playlist))
                   "Radioseven")
    (widget-insert "\n\n")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (emms-start)
                             (message "Playing..."))
                   "Play")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (emms-previous))
                   "Previous")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (emms-next)
                             (message "Next"))
                   "Next")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (emms-pause)
                             (message "Pause"))
                   "Pause")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (emms-stop)
                             (message "Stopped."))
                   "Stop")
    (widget-insert "  ")
    (widget-create 'push-button
                   :notify (lambda (&rest ignore)
                             (my-emms-ui-show-playlist))
                   "Show playlist")
    (widget-insert "\n")
    (use-local-map widget-keymap)
    (widget-setup)
    (goto-char (point-min))))


;;;;;;;fin el widget ese...


;; Mingus
(add-to-list 'load-path "~/.emacs.d/mingus")
(require 'mingus)
(autoload 'mingus "mingus-stays-home")



;;;;;;;;;;;;;;;
;; nXhtml
;;   (load "~/.emacs.d/nxhtml/autostart.el")


;;;;;;;;;;
;; multiwebmode
;; (add-to-list 'load-path "~/.emacs.d/multi-web-mode")
;; (require 'multi-web-mode)
;; (setq mweb-default-major-mode 'html-mode)
;; (setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?=" "\\?>")
;;                   (javascript-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
;;                   (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")))
;; (setq mweb-filename-extensions '("php" "htm" "html" "ctp" "phtml" "php4" "php5"))
;; (multi-web-global-mode 1)

;;rainbow mode
;(add-to-list 'load-path "~/.emacs.d/jd-el")
;(require 'rainbow-mode)
(load "~/.emacs.d/jd-el/rainbow-mode.el")
;(rainbow-mode)


;;;;;;;;;;;
;; diccionario-traductor
(defun diccionario-traductor (query)
  "Translate word at point"
  (interactive "sTranslate: ")
  (let* ((url
          (concat "http://diccionario.reverso.net/ingles-espanol/" 
		  query))
         (definition
           (progn
             (set-buffer (url-retrieve-synchronously url))
             (goto-char (point-min))
             (buffer-substring-no-properties 
              (search-forward "direction=\"targettarget\">") 
              (- (search-forward "<span") 5)) )))
    (message "%s: %s" query definition)))

;;; atajo para traductor
(global-set-key "\C-xt" 'diccionario-traductor)


;; Load ERC
;(add-to-list 'load-path "~/Descargas/erc/")
;(load "~/Descargas/emacs/erc/erc.el")
;(require 'erc)
;(require 'erc-nicklist)
(erc-spelling-mode 1)
;; Load authentication info from an external source.  Put sensitive
;; passwords and the like in here.
(setq erc-email-userid "tsolar")
(setq erc-auto-query 'buffer)
(load "~/.emacs.d/erc-auth")
 
;; Join channels whenever connecting to Freenode.
(setq erc-autojoin-channels-alist '(("freenode.net" "#parabola" "#fsfla" "#flisol-cl")
                                    ("partidopirata.cl" "#ppirata-cl" )
                                   )
)
;; Interpret mIRC-style color commands in IRC chats
(setq erc-interpret-mirc-color t)
      
;; Kill buffers for channels after /part
(setq erc-kill-buffer-on-part t)
;; Kill buffers for private queries after quitting the server
(setq erc-kill-queries-on-quit t)
;; Kill buffers for server messages after quitting the server
(setq erc-kill-server-buffer-on-quit t)


;; timestamps                                                                                     
(make-variable-buffer-local
 (defvar erc-last-datestamp nil))

(defun ks-timestamp (string)
  (erc-insert-timestamp-left string)
  (let ((datestamp (erc-format-timestamp (current-time) erc-datestamp-format)))
    (unless (string= datestamp erc-last-datestamp)
      (erc-insert-timestamp-left datestamp)
      (setq erc-last-datestamp datestamp))))
(setq erc-fill-static-center 15)    

(setq erc-timestamp-only-if-changed-flag t
      erc-timestamp-format "%H:%M:%S "
      erc-datestamp-format "=== [%Y-%m-%d %a] ===\n" ; mandatory ascii art                          
      erc-fill-prefix      "         "
      erc-insert-timestamp-function 'ks-timestamp)

(load "~/.emacs.d/erc-highlight-nicknames.el")
(erc-highlight-nicknames-mode )
(require 'erc-highlight-nicknames)

(add-hook 'window-configuration-change-hook 
	   '(lambda ()
	      (setq erc-fill-column (- (window-width) 2))))

(require 'erc-nick-notify)
;(require 'erc-tab)

;;colores!!!!!
(set-face-foreground 'erc-input-face "gold")
(set-face-foreground 'erc-my-nick-face "gold")
(set-face-foreground 'erc-timestamp-face "cyan")

(defvar erc-channels-to-visit nil
   "Channels that have not yet been visited by erc-next-channel-buffer")
(defun erc-next-channel-buffer ()
  "Switch to the next unvisited channel. See erc-channels-to-visit"
  (interactive)
  (when (null erc-channels-to-visit)
    (setq erc-channels-to-visit 
          (remove (current-buffer) (erc-channel-list nil))))
          (let ((target (pop erc-channels-to-visit)))
          (if target 
              (switch-to-buffer target)
          )
    )
)

;; contar ops voices y members..
(define-minor-mode ncm-mode "" nil
  (:eval
   (let ((ops 0)
         (voices 0)
         (members 0))
     (maphash (lambda (key value)
                (when (erc-channel-user-op-p key)
                  (setq ops (1+ ops)))
                (when (erc-channel-user-voice-p key)
                  (setq voices (1+ voices)))
                (setq members (1+ members)))
              erc-channel-users)
     (format " %S/%S/%S" ops voices members))))

(add-hook 'erc-mode-hook 'ncm-mode)
;;fin erc




;;;jabber
(require 'jabber)

(setq jabber-account-list '(
                            ;("tsolar@gmail.com/emacs"
                            ;;   (:password . nil) or (:password . "your-pass")
                             ; (:network-server . "talk.google.com")
                             ; (:port . 5223)
                             ; (:connection-type . ssl)
                            ;)
                            ("tsolar@127.0.0.1/emacs"
                              ;(:network-server . "localhost")
                              ;(:port . 5222)
                              ;(:connection-type . ssl)
                            )
                            ;("tsolar@chat.facebook.com/emacs")
                           )
)


;;;elscreen
(load "elscreen" "ElScreen" t)
;(setq elscreen-prefix-key “\C-z”)

(defun elscreen-frame-title-update ()
  (when (elscreen-screen-modified-p 'elscreen-frame-title-update)
    (let* ((screen-list (sort (elscreen-get-screen-list) '<))
 	   (screen-to-name-alist (elscreen-get-screen-to-name-alist))
 	   (title (mapconcat
 		   (lambda (screen)
 		     (format "%d%s %s"
 			     screen (elscreen-status-label screen)
 			     (get-alist screen screen-to-name-alist)))
 		   screen-list " ")))
      (if (fboundp 'set-frame-name)
 	  (set-frame-name title)
 	(setq frame-title-format title)))))

(eval-after-load "elscreen"
  '(add-hook 'elscreen-screen-update-hook 'elscreen-frame-title-update))

;;multiterm
(require 'multi-term)
(setq multi-term-program "/bin/bash")

;;ELIM
;(add-to-list 'load-path "~/.emacs.d/elim/elisp")
;(autoload 'garak "garak" nil t)
;(require 'garak)
;(setq elim-executable "/usr/bin/elim-client")
;(setq elim-executable "~/.emacs.d/elim/elim-client")


;; BBDB
;;----------------------------
;(add-to-list 'load-path "~/.emacs.d/bbdb/lisp")
;; (require 'bbdb)
;; (bbdb-initialize 'gnus 'message)
;; (add-hook 'mail-setup-hook 'bbdb-insinuate-sendmail)
;; (add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
;; ;(add-hook 'message-setup-hook 'bbdb-define-all-aliases) ; deprecated
;; (add-hook 'message-setup-hook 'bbdb-get-mail-aliases)
;; (setq bbdb-send-mail-style 'message)




;(defun irc-elscreen ()
;  "Creates a new screen for each irc channel"
;  (interactive)
;  (select-frame-by-name "Emacs IRC")  
;  (let ((lista (mapcar (function buffer-name) (buffer-list)));
;	(first-buff (buffer-name)))
;    (dolist (buff lista) 
;      (if (and (string-match "^[#][.]*" buff) (not (string= first-buff buff)));
;	  ((lambda ()
;	     (elscreen-create)
;	     (switch-to-buffer buff)))))))

;(defun elscreen-kill-and-buffer ()
;  "Kills the current tab and also the buffer that is editing. If
;is just one screen, only kills the buffer"
;  (interactive)
;  (let ((cur-buf (buffer-name)))
;    (kill-buffer (buffer-name))
;    (if (> (elscreen-get-number-of-screens) 1)
;	(elscreen-kill))))

;;circe...
(add-to-list 'load-path "~/.emacs.d/circe")
(autoload 'circe "circe" "Connect to an IRC server" t)


 ;; display-time-mode mail notification
; (defface display-time-mail-face '((t (:background "red")))
;     "If display-time-use-mail-icon is non-nil, its background colour is that
;      of this face. Should be distinct from mode-line. Note that this does not seem
;      to affect display-time-mail-string as claimed.")
; (setq
;  display-time-mail-file "~/Mail"
;  display-time-use-mail-icon t
;  display-time-mail-face 'display-time-mail-face)
; (display-time-mode t)

; (setq gnus-important-group-level 1)
; (defun gnus-new-important-mail (&optional level)
;  "Return t there is any new mail/news in groups at or below
;  level LEVEL (defaults to gnus-important-group-level)."
;  (or level (setq level gnus-important-group-level))
;  (let ((newmail nil))
;    (save-window-excursion
;      (if (and (file-exists-p display-time-mail-file)
;               (> (nth 7 (file-attributes display-time-mail-file)) 0))
;          (gnus-group-get-new-news level))
;      (switch-to-buffer gnus-group-buffer)
;      (gnus-group-list-groups level)
;      (let ((groups (gnus-group-listed-groups)) group)
;        (while (setq group (pop groups))
;          (unless newmail
;            (gnus-group-goto-group group)
;            (setq newmail (> (gnus-group-group-unread) 0)))))
;      (gnus-group-list-groups gnus-level-subscribed))
;    newmail))
; (setq display-time-mail-function 'gnus-new-important-mail)

;; Copy mail-notify.el to your load-path and add to your ~/.emacs
;;
;(require 'mail-notify)
;;
;; Startup mail-notify timer, add blow in your ~/.emacs
;(run-with-timer 0 mail-notify-repeat 'mail-notify)
;;
;; Set new mail storage directory
;(setq mail-notify-directory "~/Mail")

;(require 'notify)
;(setq gnus-group-get-new-news-finish-function 

(autoload 'pkgbuild-mode "pkgbuild-mode.el" "PKGBUILD mode." t)
(setq auto-mode-alist (append '(("/PKGBUILD$" . pkgbuild-mode)) auto-mode-alist))

;;symfony
;(add-to-list 'load-path "~/.emacs.d/emacs-symfony")
;(require 'symfony)

;; (autoload 'wl "wl" "Wanderlust" t)


;(require 'web-vcs)
(load "~/.emacs.d/nxhtml/autostart.el")
;(load "YOUR-PATH-TO/nxhtml/autostart.el")

;; gnus init file
(setq gnus-init-file "~/.emacs.d/.gnus.el")
